#include <Rcpp.h>
#include <stdio.h>
using namespace Rcpp;

struct cho_node_tasks{
  int dpotrf=0;
  int dsyrk=0;
  int dtrsm=0;
  int dgemm=0;
};

// [[Rcpp::export]]
DataFrame cho_tasks_per_node_c(IntegerMatrix dist, int nodes, int number_blocks, int it=-1, int start=1) {

   if(it==-1){
      it = number_blocks;
   }

   cho_node_tasks* tasks_nodes = new cho_node_tasks[nodes];

   int node;
   //printf("1\n");
   if(number_blocks>0){
     for(int k=start-1; k<it; k++){
        node = dist(k, k);
        if(!IntegerVector::is_na(node))
            tasks_nodes[node-1].dpotrf++;
        if(k < number_blocks){
          for(int m=k+1; m<number_blocks; m++){
              node = dist(k, m);
              if(!IntegerVector::is_na(node))
                 tasks_nodes[node-1].dtrsm++;
          }


          for(int n=k+1; n<number_blocks; n++){
              node = dist(n, n);
              if(!IntegerVector::is_na(node))
                 tasks_nodes[node-1].dsyrk++;
              for(int m=n+1; m<number_blocks; m++){
                 node = dist(n, m);
                 if(!IntegerVector::is_na(node))
                    tasks_nodes[node-1].dgemm++;
              }
          }
        }
     }
   }
   //printf("2\n");
   IntegerVector Node(nodes*4);
   IntegerVector Value(nodes*4);
   IntegerVector freq(nodes*4);

   for(int n=0; n<nodes;n++){
     Node[n*4] = n;
     Value[n*4] = 1;
     freq[n*4] = tasks_nodes[n].dgemm;

     Node[n*4 + 1] = n;
     Value[n*4 + 1] = 2;
     freq[n*4 + 1] = tasks_nodes[n].dpotrf;

     Node[n*4 + 2] = n;
     Value[n*4 + 2] = 3;
     freq[n*4 + 2] = tasks_nodes[n].dsyrk;

     Node[n*4 + 3] = n;
     Value[n*4 + 3] = 4;
     freq[n*4 + 3] = tasks_nodes[n].dtrsm;


   }

   CharacterVector lvl = {"dgemm", "dpotrf", "dsyrk", "dtrsm"};

   Value.attr("class") = "factor";
   Value.attr("levels") = lvl;

   //printf("3\n");
   DataFrame df = DataFrame::create( Named("Node") = Node,
                                   _["Value"] = Value,
                                   _["freq"] = freq );

   return df;
}


struct cho_cmg_node_tasks{
  int dcmg=0;
  int dpotrf=0;
  int dsyrk=0;
  int dtrsm=0;
  int dgemm=0;
};

// [[Rcpp::export]]
DataFrame cho_cmg_tasks_per_node_c(IntegerMatrix dist, IntegerVector generation, int nodes, int number_blocks, int it=-1, int start=1) {

   if(it==-1){
      it = number_blocks;
   }

   cho_cmg_node_tasks* tasks_nodes = new cho_cmg_node_tasks[nodes];

   int node;
   //printf("1\n");
   if(number_blocks>0){

          for(int n=0; n<nodes; n++){
             tasks_nodes[n].dcmg = generation[n];
          }

     for(int k=start-1; k<it; k++){
        node = dist(k, k);
        if(!IntegerVector::is_na(node))
            tasks_nodes[node-1].dpotrf++;
        if(k < number_blocks){
          for(int m=k+1; m<number_blocks; m++){
              node = dist(k, m);
              if(!IntegerVector::is_na(node))
                 tasks_nodes[node-1].dtrsm++;
          }


          for(int n=k+1; n<number_blocks; n++){
              node = dist(n, n);
              if(!IntegerVector::is_na(node))
                 tasks_nodes[node-1].dsyrk++;
              for(int m=n+1; m<number_blocks; m++){
                 node = dist(n, m);
                 if(!IntegerVector::is_na(node))
                    tasks_nodes[node-1].dgemm++;
              }
          }
        }
     }
   }
   //printf("2\n");
   IntegerVector Node(nodes*5);
   IntegerVector Value(nodes*5);
   IntegerVector freq(nodes*5);

   for(int n=0; n<nodes;n++){
     Node[n*5] = n;
     Value[n*5] = 1;
     freq[n*5] = tasks_nodes[n].dcmg;

     Node[n*5 + 1] = n;
     Value[n*5 + 1] = 2;
     freq[n*5 + 1] = tasks_nodes[n].dgemm;

     Node[n*5 + 2] = n;
     Value[n*5 + 2] = 3;
     freq[n*5 + 2] = tasks_nodes[n].dpotrf;

     Node[n*5 + 3] = n;
     Value[n*5 + 3] = 4;
     freq[n*5 + 3] = tasks_nodes[n].dsyrk;

     Node[n*5 + 4] = n;
     Value[n*5 + 4] = 5;
     freq[n*5 + 4] = tasks_nodes[n].dtrsm;


   }

   CharacterVector lvl = {"dcmg", "dgemm", "dpotrf", "dsyrk", "dtrsm"};

   Value.attr("class") = "factor";
   Value.attr("levels") = lvl;

   //printf("3\n");
   DataFrame df = DataFrame::create( Named("Node") = Node,
                                   _["Value"] = Value,
                                   _["freq"] = freq );

   return df;
}
